//
//  ViewController.m
//  RecipeAssignment
//
//  Created by Douglas Sass on 2/8/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Recipes";
    
    recipesArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RecipesPList" ofType:@"plist"]];
    
//    NSString* path = [[NSBundle mainBundle]pathForResource:@"RecipesPList" ofType:@"plist"];
//    recipes = [[NSArray alloc]initWithContentsOfFile:path];
//    recipes = @[@"Test"];
//    
//    recipeTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
//    recipeTableView.delegate = self;
//    recipeTableView.dataSource = self;
//
//    [self.view addSubview:recipeTableView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return recipesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
    
    NSDictionary* d = recipesArray[indexPath.row];
    
    cell.textLabel.text = d[@"recipeTitle"];
    
    return cell;
    
    }

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"recipeSegue" sender:indexPath];
    
//    NSDictionary* dictionary = recipes[indexPath.row];
//    
//    ReciepeDetailsViewController* rdvc = [ReciepeDetailsViewController new];
//    
//    rdvc.title.text = dictionary[@"title"];
//    rdvc.image. = dictionary[@"image"];
//    rdvc.ingredients.text = dictionary[@"ingredients"];
//    rdvc.instructions.text = dictionary[@"instructions"];
//    
//    [self.navigationController pushViewController:rdvc animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath* ip = (NSIndexPath*)sender;
    if ([segue.identifier isEqualToString:@"recipeSegue"]) {
        ReciepeDetailsViewController* rdvc = (ReciepeDetailsViewController*)segue.destinationViewController;
        rdvc.recipesInformation = recipesArray[ip.row];
    }
}


    
    
    
@end
