//
//  ViewController.h
//  RecipeAssignment
//
//  Created by Douglas Sass on 2/8/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReciepeDetailsViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    NSArray* recipesArray;
}


@end

